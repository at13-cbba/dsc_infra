#!/bin/bash

sudo apt-get update
sudo apt install openjdk-11-jre-headless -y
sudo apt install openjdk-11-jdk-headless -y
sudo apt install ffmpeg -y
sudo curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs
git clone https://github.com/prog102-AT13/JalaConverter.git
mkdir JalaConverter/archive
mkdir JalaConverter/archive/storage
cd JalaConverter/
git checkout dev-ops
cd docker/
docker-compose -f dockercompose.yml up -d
cd ..
sudo ./gradlew bootRun